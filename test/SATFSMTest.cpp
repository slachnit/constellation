// SPDX-FileCopyrightText: 2022 Stephan Lachnit
// SPDX-License-Identifier: EUPL-1.2

#include <iostream>
#include <memory>
#include "magic_enum.hpp"
#include "Constellation/SAT/Satellite.hpp"

using namespace Constellation;

int main() {
    auto satellite = std::make_shared<Satellite>("test");

    while (true) {
        std::cout << "State: " << magic_enum::enum_name(satellite->GetFSMState()) << std::endl;
        std::string event_name;
        std::cout << "Event: ";
        std::cin >> event_name;
        auto event = magic_enum::enum_cast<SatEvent>(event_name);

        if (event.has_value()) {
            try {
                satellite->FSMReact(event.value(), nullptr);
            }
            catch(const SerializibleFSM::ReactionError& error) {
                std::cerr << error.what() << std::endl;
            }
        }
        else {
            std::cerr << "Invalid event name" << std::endl;
        }
    }

    return 0;
}
