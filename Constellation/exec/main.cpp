// SPDX-FileCopyrightText: 2022 Stephan Lachnit
// SPDX-License-Identifier: EUPL-1.2

#include <iostream>
#include <cstring>

#include "zmq.hpp"

#include "Constellation/RC/RCLoop.hpp"
#include "Constellation/SAT/SatLoop.hpp"
#include "Constellation/SAT/Satellite.hpp"

using namespace Constellation;

int main(int argc, char* argv[]) {
    zmq::context_t context {1};
    if (argc == 3) {
        if (strcmp(argv[1], "RC") == 0) {
            std::cout << "starting as RC binding to " << argv[2] << std::endl;
            RCLoop(context, argv[2]);
            return 0;
        } else if (strcmp(argv[1], "SAT") == 0) {
            std::cout << "starting as SAT connecting to " << argv[2] << std::endl;
            SatLoop(context, argv[2], std::make_shared<Satellite>("test"));
            return 0;
        } else {
            std::cout << "wrong usage" << std::endl;
            return 1;
        }
    } else {
        std::cout << "wrong usage" << std::endl;
        return 1;
    }
}
