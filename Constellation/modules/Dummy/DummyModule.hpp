// SPDX-FileCopyrightText: 2022 Stephan Lachnit
// SPDX-License-Identifier: EUPL-1.2

#pragma once

#include "Constellation/SAT/SatModule.hpp"

#include <iostream>

namespace Constellation {
    class DummyModule : public SatModule {
    public:
        DummyModule() : SatModule() { std::cout << "[DummyModule]: Created" << std::endl; };
        virtual ~DummyModule() { std::cout << "[DummyModule]: Destroyed" << std::endl; };
        void Configure() override { std::cout << "[DummyModule]: Configure" << std::endl; };
        void Reconfigure() override { std::cout << "[DummyModule]: Reconfigure" << std::endl; Configure(); };
        void Deconfigure() override { std::cout << "[DummyModule]: Deconfigure" << std::endl; };
        void PowerUp() override { std::cout << "[DummyModule]: PowerUp" << std::endl; };
        void PowerDown() override { std::cout << "[DummyModule]: PowerDown" << std::endl; };
        void Run() override;
    };
}
