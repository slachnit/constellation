// SPDX-FileCopyrightText: 2022 Stephan Lachnit
// SPDX-License-Identifier: EUPL-1.2

#include "DummyModule.hpp"

#include <chrono>
#include <thread>

using namespace Constellation;

void Constellation::DummyModule::Run() {
    while (stop_run_ == false) {
        std::cout << "[DummyModule]: Running..." << std::endl;
        std::this_thread::sleep_for(std::chrono::seconds(1));
    }
    std::cout << "[DummyModule]: Stop running" << std::endl;
}

// generator function for dynamic module loading
extern "C" std::shared_ptr<Constellation::SatModule> moduleGenerator() {
    return static_cast<std::shared_ptr<Constellation::SatModule>>(std::make_shared<DummyModule>());
}
