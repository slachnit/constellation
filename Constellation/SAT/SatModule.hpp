// SPDX-FileCopyrightText: 2022 Stephan Lachnit
// SPDX-License-Identifier: EUPL-1.2

#pragma once

#include <atomic>
#include <memory>
#include <thread>

namespace Constellation {
    class SatModule {
    public:
        virtual ~SatModule() = default;
        // Set configuration
        void SetConfig(void* config) { config_ = config; };
        // Configure from unconfigured state
        virtual void Configure() = 0;
        // Reconfigure (from configured state)
        virtual void Reconfigure() { Configure(); };
        // Deconfigure, called when modules is terminated (e.g. turn off PSU)
        virtual void Deconfigure() {};
        // Power up, i.e. make module ready for run (e.g. turn on HV, allocate buffers, ...)
        virtual void PowerUp() = 0;
        // Power down (e.g. turn off HV, free buffers, ...)
        virtual void PowerDown() = 0;
        // Launch Run thread asynchronous
        void LaunchRun();
        // Tell Run() to stop running
        void StopRun();
    protected:
        // private constructor
        SatModule() : config_(nullptr) {};
        // Function executed during run, needs to stop when stop_run_ is true
        virtual void Run() = 0;
        // If true, Run() needs to exit
        std::atomic_bool stop_run_;
        // Module configuration
        void* config_;
    private:
        void RunWrapper();
        std::thread run_thread_;
    };

    typedef std::shared_ptr<SatModule> SatModuleGenerator();
}
