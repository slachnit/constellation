// SPDX-FileCopyrightText: 2022 Stephan Lachnit
// SPDX-License-Identifier: EUPL-1.2

#pragma once

#include <memory>
#include <string>

#include "Constellation/SAT/SatModule.hpp"
#include "Constellation/SAT/SatFSM.hpp"
#include "Constellation/core/DSOLoader.hpp"

namespace Constellation {
    class Satellite {
    public:
        Satellite(const std::string& name) : name_(name), fsm_(createSatFSM()) {};
        virtual ~Satellite() {};
        const std::string& GetName() const { return name_; };
        void FSMReact(SatEvent event, void* msg) { fsm_.React(event, this, msg); };
        SatState GetFSMState() const { return fsm_.GetCurrentState(); };
        std::shared_ptr<SatModule>& GetModule();
        void CreateModule(const std::string& module_name);
        void DeleteModule();
    private:
        std::string name_;
        SatFSM fsm_;
        std::shared_ptr<DSOLoader> dso_loader_;
        std::shared_ptr<SatModule> module_;
    };
}
