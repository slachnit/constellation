// SPDX-FileCopyrightText: 2022 Stephan Lachnit
// SPDX-License-Identifier: EUPL-1.2

#pragma once

#include <string>

#include "zmq.hpp"

#include "Constellation/SAT/Satellite.hpp"

namespace Constellation {
    void SatLoop(zmq::context_t& context, const std::string& rc_addr, std::shared_ptr<Satellite> satellite);
}
