// SPDX-FileCopyrightText: 2022 Stephan Lachnit
// SPDX-License-Identifier: EUPL-1.2

#pragma once

#include "SerializableFSM.hpp"

namespace Constellation {
    class Satellite;  // forward declaration

    // Possible states
    enum class SatState {
        Idle,             // No connection to RC
        Launched,         // Connection to RC, Module is created
        Configured,       // Module is configured
        Ready,            // Module is ready to run
        Running,          // Module is running
        Error,            // Critical error state, needs manual resolving
    };
    // Possible events
    enum class SatEvent {
        Connect,          // RC address found, create Modules
        Configure,
        PowerUp,
        PowerDown,
        StartRun,
        StopRun,
        Interrupt,         // Critical error, needs immediate powerDown, e.g. hardware failure
        ConnectionLost,    // Connection to RC lost, go to Idle
        ResetRequest,      // If in Error state, User emits this event to go back to Launched
    };

    using SatEventReaction = SerializibleFSM::EventReaction<SatState, Satellite*, void*>;
    using SatEventReactionMap = SerializibleFSM::EventReactionMap<SatState, SatEvent, Satellite*, void*>;
    using SatStateMap = SerializibleFSM::StateMap<SatState, SatEvent, Satellite*, void*>;
    using SatFSM = SerializibleFSM::FSM<SatState, SatEvent, Satellite*, void*>;

    // Create FSM starting in Idle state
    SatFSM createSatFSM();
}
