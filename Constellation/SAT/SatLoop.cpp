// SPDX-FileCopyrightText: 2022 Stephan Lachnit
// SPDX-License-Identifier: EUPL-1.2

#include "Constellation/SAT/SatLoop.hpp"

#include <string>
#include <memory>
#include <iostream>

#include "magic_enum.hpp"
#include "zmq.hpp"

#include "Constellation/SAT/SatFSM.hpp"

using namespace Constellation;


void Constellation::SatLoop(zmq::context_t& context, const std::string& rc_addr, std::shared_ptr<Satellite> satellite) {
    // Connect to RC via REP
    zmq::socket_t socket_rep {context, zmq::socket_type::rep};
    socket_rep.connect(rc_addr);

    // Assume we are connected - fix exact working later
    satellite->FSMReact(SatEvent::Connect, nullptr);

    // receive messages in loop
    while (true) {
        zmq::message_t msg;
        auto msg_res = socket_rep.recv(msg);
        if (msg_res.has_value()) {
            auto msg_str = msg.to_string_view();

            // Log message
            std::cout << "Debug: Got " << msg_str << std::endl;

            // cast message to SatEvent enum and react
            auto event = magic_enum::enum_cast<SatEvent>(msg_str);
            if (event.has_value()) {
                try {
                    satellite->FSMReact(event.value(), nullptr);
                }
                catch(const SerializibleFSM::ReactionError& error) {
                    std::cerr << "Error: " << error.what() << std::endl;
                }
            } else {
                std::cerr << "Error: got invalid event" << std::endl;
            }

            // reply with FSM state
            std::cout << "FSM state: " << magic_enum::enum_name(satellite->GetFSMState()) << std::endl;
            auto send_res = socket_rep.send(zmq::buffer(magic_enum::enum_name(satellite->GetFSMState())));
            if (!send_res.has_value()) {
                std::cerr << "Error: failed sending reply" << std::endl;
            }
        } else {
            std::cerr << "Error: message has no value" << std::endl;
        }
    }
}
