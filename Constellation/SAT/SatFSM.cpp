// SPDX-FileCopyrightText: 2022 Stephan Lachnit
// SPDX-License-Identifier: EUPL-1.2

#include "Constellation/SAT/SatFSM.hpp"

#include "Constellation/SAT/Satellite.hpp"

using namespace Constellation;

SatState connect(Satellite* satellite, void*) {
    // get module name from void*, see below
    satellite->CreateModule("Dummy");
    return SatState::Launched;
}
SatState configure(Satellite* satellite, void* msg) {
    // Fixme: replace void* with zmq::message_t
    // Then decode config from message
    // essentially, a MessageManager recvs a multipart msg
    // with the first msg the SatEvent
    // and the second msg some user object we will unpack here
    satellite->GetModule()->SetConfig(msg);
    satellite->GetModule()->Configure();
    return SatState::Configured;
}
SatState reconfigure(Satellite* satellite, void* msg) {
    // see above
    satellite->GetModule()->SetConfig(msg);
    satellite->GetModule()->Reconfigure();
    return SatState::Configured;
}
SatState powerUp(Satellite* satellite, void*) {
    satellite->GetModule()->PowerUp();
    return SatState::Ready;
}
SatState powerDown(Satellite* satellite, void*) {
    satellite->GetModule()->PowerDown();
    return SatState::Configured;
}
SatState startRun(Satellite* satellite, void*) {
    satellite->GetModule()->LaunchRun();
    return SatState::Running;
}
SatState stopRun(Satellite* satellite, void*) {
    satellite->GetModule()->StopRun();
    return SatState::Ready;
}
SatState interrupt(Satellite*, void*) {
    // REQ interrupt to RC
    return SatState::Error;
}
SatState interruptPwr(Satellite* satellite, void* msg) {
    satellite->GetModule()->PowerDown();
    return interrupt(satellite, msg);
}
SatState interruptRun(Satellite* satellite, void* msg) {
    satellite->GetModule()->StopRun();
    return interruptPwr(satellite, msg);
}
SatState connectionLost(Satellite* satellite, void*) {
    satellite->DeleteModule();
    return SatState::Idle;
}
SatState connectionLostConf(Satellite* satellite, void* msg) {
    satellite->GetModule()->Deconfigure();
    return connectionLost(satellite, msg);
}
SatState connectionLostPwr(Satellite* satellite, void* msg) {
    satellite->GetModule()->PowerDown();
    return connectionLostConf(satellite, msg);
}
SatState connectionLostRun(Satellite* satellite, void* msg) {
    satellite->GetModule()->StopRun();
    return connectionLostPwr(satellite, msg);
}
SatState resetError(Satellite*, void*) {
    return SatState::Launched;
}

SatFSM Constellation::createSatFSM() {
    // Definition of state map
    SatStateMap state_map {
        {SatState::Idle, SatEventReactionMap {
            {SatEvent::Connect, connect},
        }},
        {SatState::Launched, SatEventReactionMap {
            {SatEvent::Configure, configure},
            {SatEvent::ConnectionLost, connectionLost},
        }},
        {SatState::Configured, SatEventReactionMap {
            {SatEvent::Configure, reconfigure},
            {SatEvent::PowerUp, powerUp},
            {SatEvent::Interrupt, interrupt},
            {SatEvent::ConnectionLost, connectionLostConf},
        }},
        {SatState::Ready, SatEventReactionMap {
            {SatEvent::PowerDown, powerDown},
            {SatEvent::StartRun, startRun},
            {SatEvent::Interrupt, interruptPwr},
            {SatEvent::ConnectionLost, connectionLostPwr},
        }},
        {SatState::Running, SatEventReactionMap {
            {SatEvent::StopRun, stopRun},
            {SatEvent::Interrupt, interruptRun},
            {SatEvent::ConnectionLost, connectionLostRun},
        }},
        {SatState::Error, SatEventReactionMap {
            {SatEvent::ResetRequest, resetError},
        }},
    };
    // Return FSM starting in Idle state
    return SatFSM {state_map, SatState::Idle};
}
