// SPDX-FileCopyrightText: 2022 Stephan Lachnit
// SPDX-License-Identifier: EUPL-1.2

#include "Constellation/SAT/Satellite.hpp"

using namespace Constellation;

std::shared_ptr<SatModule>& Constellation::Satellite::GetModule() {
    if (module_ == nullptr) {
        // throw
    }
    return module_;
}

void Constellation::Satellite::CreateModule(const std::string& module_name) {
    dso_loader_ = std::make_shared<DSOLoader>(module_name + "Module");
    auto* moduleGenerator = dso_loader_->GetFunctionFromDSO<SatModuleGenerator>("moduleGenerator");
    module_ = moduleGenerator();
}

void Constellation::Satellite::DeleteModule() {
    module_.reset();
    dso_loader_.reset();
}
