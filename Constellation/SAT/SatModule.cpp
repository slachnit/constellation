// SPDX-FileCopyrightText: 2022 Stephan Lachnit
// SPDX-License-Identifier: EUPL-1.2

#include "Constellation/SAT/SatModule.hpp"

using namespace Constellation;

void Constellation::SatModule::RunWrapper() {
    try {
        Run();
    }
    catch(const std::exception& error) {
        // if Run() throws an error we have to handle it here since the thread runs detached
        // what we should do is call some kind of FSM Manager to get the FSM and then call
        // fsm->React(SatState::Interrupt, this)
        // Problem: we don't really know our FSM here point because we are in core
        // Solution: move to SAT, and call LaunchRun(std::shared_ptr<SatFSM> fsm)
        //  and then RunWrapper(std::shared_ptr<SatFSM> fsm)
    }
}

void Constellation::SatModule::LaunchRun() {
    stop_run_ = false;
    run_thread_ = std::thread(&SatModule::RunWrapper, this);
}

void Constellation::SatModule::StopRun() {
    stop_run_ = true;
    run_thread_.join();
}
