// SPDX-FileCopyrightText: 2022 Stephan Lachnit
// SPDX-License-Identifier: EUPL-1.2

#pragma once

#include <exception>
#include <string>

namespace Constellation {
    class DSOLoaderError : public std::exception {
    public:
        explicit DSOLoaderError(std::string error_message) : error_message_(error_message) {}
        const char* what() const noexcept override { return error_message_.c_str(); }
    protected:
        std::string error_message_;
    };
}
