// SPDX-FileCopyrightText: 2022 Stephan Lachnit
// SPDX-License-Identifier: EUPL-1.2

#include "Constellation/core/DSOLoader.hpp"

#include <dlfcn.h>

#include "Constellation/core/error.hpp"
#include "Constellation/core/config.hpp"

using namespace Constellation;

Constellation::DSOLoader::DSOLoader(const std::string& dso_name) {
    auto path_str = CONSTELLATION_INSTALL_FULL_LIBDIR "/ConstellationModules/lib" + dso_name + ".so";

    // Note: we don't actually need to check if the file exists, dlopen already does that and
    // dlerror() gives a good human-readable error in that case which we throw anyway

    handle_ = dlopen(path_str.c_str(), RTLD_NOW);
    auto* error_dlopen = dlerror();
    if (handle_ == nullptr || error_dlopen != nullptr) {
        throw DSOLoaderError(error_dlopen);
    }
}

Constellation::DSOLoader::~DSOLoader() {
    dlclose(handle_);
}

void* Constellation::DSOLoader::GetRawFunctionFromDSO(const std::string& function_name) {
    void* function = dlsym(handle_, function_name.c_str());
    auto* error_dlsym = dlerror();
    if (function == nullptr || error_dlsym != nullptr) {
        throw DSOLoaderError(error_dlsym);
    }
    return function;
}
