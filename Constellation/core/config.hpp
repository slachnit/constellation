// SPDX-FileCopyrightText: 2022 Stephan Lachnit
// SPDX-License-Identifier: EUPL-1.2

#pragma once

#define CONSTELLATION_VERSION "@version@"

#define CONSTELLATION_INSTALL_FULL_LIBDIR "@install_full_libdir@"
