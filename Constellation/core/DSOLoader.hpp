// SPDX-FileCopyrightText: 2022 Stephan Lachnit
// SPDX-License-Identifier: EUPL-1.2

#pragma once

#include <string>

namespace Constellation {
    class DSOLoader {
    public:
        DSOLoader(const std::string& dso_name);
        virtual ~DSOLoader();

        /**
         * @brief Returns function pointer to function from DSO
         * @tparam FunctionType the typedef of the function
         * @param function_name the name of the function
         */
        template<typename FunctionType>
        FunctionType* GetFunctionFromDSO(const std::string& function_name) {
            return reinterpret_cast<FunctionType*>(GetRawFunctionFromDSO(function_name));
        }
    protected:
        void* GetRawFunctionFromDSO(const std::string& function_name);
    private:
        void* handle_;
    };
}
