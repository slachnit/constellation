// SPDX-FileCopyrightText: 2022 Stephan Lachnit
// SPDX-License-Identifier: EUPL-1.2

#pragma once

#include <string>

#include "zmq.hpp"

namespace Constellation {
    void RCLoop(zmq::context_t& context, const std::string& sat_addr);
}
