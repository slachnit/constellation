// SPDX-FileCopyrightText: 2022 Stephan Lachnit
// SPDX-License-Identifier: EUPL-1.2

#include "Constellation/RC/RCLoop.hpp"

#include <iostream>
#include <string>

#include "zmq.hpp"

using namespace Constellation;


void Constellation::RCLoop(zmq::context_t& context, const std::string& sat_addr) {
    // Bind socket (only one RC allowed)
    zmq::socket_t socket_req {context, zmq::socket_type::req};
    socket_req.bind(sat_addr);

    while (true) {
        std::cout << "Send message: ";
        std::string msg_str;
        std::cin >> msg_str;

        auto send_res = socket_req.send(zmq::buffer(msg_str));
        if (send_res.has_value()) {

            zmq::message_t msg_resp;
            auto msg_resp_res = socket_req.recv(msg_resp);
            if (msg_resp_res.has_value()) {
                auto msg_resp_str = msg_resp.to_string_view();
                std::cout << "Reply: " << msg_resp_str << std::endl;
            } else {
                std::cerr << "Error: failed getting reply" << std::endl;
            }
        } else {
            std::cerr << "Error: failed sending message" << std::endl;
        }
    }
}
